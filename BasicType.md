# TypeScript

Basic Types
> Boolean
- ชนิดข้อมูลประเภท Boolean

```
let isPresent: boolean = true
```
> Number
```
let decimal: number = 6
let hex: number = 0xf00d
let binary: number = 0b1010
let octal: number = 0o744
```
> String
```
let name: string = "Hello World"
```
> Array 
- ชนิดข้อมูลว่าเป็นอาร์เรย์ได้ด้วยสองวิธีต่อจากนี้
```
// วิธีที่ 1 ชนิดข้อมูลแล้วตามด้วย []
let listNumber: number[] = [1, 2, 3]

// วิธีที่ 2 Array<ชนิดข้อมูล>
let listNumber: Array<number> = [1, 2, 3]
```
> Tuple
- คือการกำหนดกำหนดชนิดข้อมูลใน Array ที่สร้างขึ้น
```
let employee: [string, number, boolean] = ["name", 30, true];
```
> Enum
- ช่วยให้เรากำหนดข้อความแทนตัวเลข ได้ดังนี้
```
// Enum จะเริ่มต้นที่ 0 ถ้าเราไม่กำหนดค่าอะไรเลย จะได้ว่า
// Red = 0, Green = 1, Blue = 2
// แต่ถ้ากำหนด Red = 1 จะได้ Green = 2, Blue = 3
enum Color {
    Red = 1,
    Green,
    Blue
}
// วิธีเรียกใช้
console.log(Color.Red) // 1
console.log(Color[1]) // Red
```
> Any
- ชนิดข้อมูล Any สามารถใส่ value ที่เป็น Number, String, Boolean อื่นๆ ได้
```
let notSure: any = 10
let notSure: any = "ten"
let notSure: any = false
```
> void
- ใช้สำหรับว่าไม่มีการ Return ค่า ออกจาก function, method เป็นต้น
```
function warnUser(): void {
  console.log("This is my warning message");
}
```
> Never
- ใช้กับ return ค่าที่ไม่สามารถระบุได้ว่าจะออกมาเป็นอะไร
```
// Function returning never must have unreachable end point
function error(message: string): never {
  throw new Error(message);
}

// Inferred return type is never
function fail() {
  return error("Something failed");
}

// Function returning never must have unreachable end point
function infiniteLoop(): never {
  while (true) {}
}
```

> Type Assertions
- เป็นการระบุว่า value นี้มันมีค่าเป็น Type ที่ต้องการเช่น String ตัวอย่าง
```
let someValue: any = "this is a string"
// someValue ชนิดข้อมูลเป็น any เราจะไม่สามารถใช้ length ได้ เพราะใช้ได้กับ String เท่านั้น
// เราสามารถบอกมันว่านี้คือ ชนิดข้อมูลเป็น String นะ ได้สองวิธี
let strLength: number = (<string>someValue).length
// หรือ
let strLength: number = (someValue as string).length
```